import {
  createClient,
  Session,
  SupabaseClientOptions,
} from "@supabase/supabase-js";

if (!process.env.SUPABASE_URL) throw new Error("SUPABASE_URL is required");

if (!process.env.SUPABASE_SERVICE_ROLE)
  throw new Error("SUPABASE_SERVICE_ROLE is required");

const options: SupabaseClientOptions = {};

export const supabaseClient = createClient(
  process.env.SUPABASE_URL,
  process.env.SUPABASE_SERVICE_ROLE,
  options
);

export type { Session };
