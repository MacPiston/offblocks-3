import { Authenticator } from "remix-auth";

import { supabaseStrategy } from "../strategy";
import { sessionStorage } from "../session-storage";
import { Session } from "../supabase";

export const authenticator = new Authenticator<Session>(sessionStorage, {
  sessionKey: supabaseStrategy.sessionKey, // keep in sync
  sessionErrorKey: supabaseStrategy.sessionErrorKey, // keep in sync
});

// @ts-ignore
authenticator.use(supabaseStrategy);
