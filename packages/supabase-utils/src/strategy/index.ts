import { SupabaseStrategy } from "remix-auth-supabase";
import { AuthorizationError } from "remix-auth";

import { sessionStorage } from "../session-storage";
import { Session, supabaseClient } from "../supabase";

export const SESSION_KEY = "sb:session";
export const SESSION_ERROR_KEY = "sb:error";

export const supabaseStrategy = new SupabaseStrategy(
  {
    supabaseClient,
    sessionStorage,
    sessionKey: SESSION_KEY,
    sessionErrorKey: SESSION_ERROR_KEY,
  },
  // simple verify example for email/password auth
  async ({ req, supabaseClient }) => {
    const form = await req.formData();
    const email = form?.get("email");
    const password = form?.get("password");

    if (!email) throw new AuthorizationError("Email is required");
    if (typeof email !== "string")
      throw new AuthorizationError("Email must be a string");

    if (!password) throw new AuthorizationError("Password is required");
    if (typeof password !== "string")
      throw new AuthorizationError("Password must be a string");

    return supabaseClient.auth.api
      .signInWithEmail(email, password)
      .then(({ data, error }): Session => {
        if (error || !data) {
          throw new AuthorizationError(
            error?.message ?? "No user session found"
          );
        }

        return data;
      });
  }
);
