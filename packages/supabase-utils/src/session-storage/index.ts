import { createCookieSessionStorage } from "@remix-run/node";

if (process.env.AUTH_SECRET === undefined)
  throw new Error("AUTH_SECRET must be defined");

export const sessionStorage = createCookieSessionStorage({
  cookie: {
    name: "sb",
    httpOnly: true,
    path: "/",
    sameSite: "lax",
    secrets: [process.env.AUTH_SECRET], // This should be an env variable
    secure: process.env.NODE_ENV === "production",
  },
});
