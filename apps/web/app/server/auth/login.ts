import { AuthorizationError } from "remix-auth";
import { ActionFunction, json, LoaderFunction } from "@remix-run/node";
import { authenticator, supabaseStrategy } from "@packages/supabase-utils";
import { routes } from "@routing";

export const loginLoader: LoaderFunction = async ({ request }) =>
  supabaseStrategy.checkSession(request, {
    successRedirect: routes.home,
  });

export const loginAction: ActionFunction = async ({ request }) => {
  try {
    return await authenticator.authenticate("sb", request, {
      successRedirect: routes.home,
    });
  } catch (e) {
    console.log(e);

    if (e instanceof Response) return e;
    if (e instanceof AuthorizationError) throw json({ message: "siema" });
  }
};
