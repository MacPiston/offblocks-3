import { FunctionComponent, PropsWithChildren } from "react";
import { AppShell } from "@mantine/core";
import type { Session } from "@packages/supabase-utils";

import Navbar from "./navbar";

type Props = {
  session?: Session;
};

const Layout: FunctionComponent<PropsWithChildren<Props>> = ({
  session,
  children,
}) => {
  return (
    <AppShell navbarOffsetBreakpoint="sm" navbar={<Navbar session={session} />}>
      {children}
    </AppShell>
  );
};

export default Layout;
