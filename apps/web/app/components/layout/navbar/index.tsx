import { FunctionComponent, useState } from "react";
import { Code, Group, Navbar as MantineNavbar } from "@mantine/core";
import { IconLogin, IconLogout } from "@tabler/icons-react";
import { routes } from "@routing";
import { Session } from "@packages/supabase-utils";

import { useNavbarStyles } from "./styles";

type Props = {
  session?: Session;
};

const Navbar: FunctionComponent<Props> = ({ session }) => {
  const { classes, cx } = useNavbarStyles();
  const [active, setActive] = useState("Billing");

  const links = [].map((item) => (
    <a
      className={cx(classes.link, {
        [classes.linkActive]: item.label === active,
      })}
      href={item.link}
      key={item.label}
      onClick={(event) => {
        event.preventDefault();
        setActive(item.label);
      }}
    >
      <item.icon className={classes.linkIcon} stroke={1.5} />
      <span>{item.label}</span>
    </a>
  ));

  return (
    <MantineNavbar width={{ sm: 300 }} p="md" className={classes.navbar}>
      <MantineNavbar.Section grow>
        <Group className={classes.header} position="apart">
          <p>logo here</p>
          <Code className={classes.version}>v3.1.2</Code>
        </Group>
        {links}
      </MantineNavbar.Section>

      <MantineNavbar.Section className={classes.footer}>
        {session ? (
          <>
            <p>{session.user.email}</p>
            <a
              href="#"
              className={classes.link}
              onClick={(event) => event.preventDefault()}
            >
              <IconLogout className={classes.linkIcon} stroke={1.5} />
              <span>Logout</span>
            </a>
          </>
        ) : (
          <a
            href={routes.auth.login}
            className={classes.link}
            onClick={(event) => event.preventDefault()}
          >
            <IconLogin className={classes.linkIcon} stroke={1.5} />
            <span>Log In</span>
          </a>
        )}
      </MantineNavbar.Section>
    </MantineNavbar>
  );
};

export default Navbar;
