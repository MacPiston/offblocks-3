import { FunctionComponent } from "react";
import {
  Anchor,
  Button,
  Checkbox,
  Group,
  PasswordInput,
  TextInput,
} from "@mantine/core";
import { Form, useSubmit } from "@remix-run/react";
import { useForm } from "@mantine/form";
import { LoginData } from "@packages/supabase-utils";

const LoginForm: FunctionComponent = () => {
  const form = useForm<LoginData>();
  const submit = useSubmit();

  return (
    <Form
      onSubmit={form.onSubmit((_, e) =>
        submit(e.currentTarget, { method: "post", replace: true })
      )}
    >
      <TextInput
        required
        label="Email"
        placeholder="you@mantine.dev"
        {...form.getInputProps("email")}
      />
      <PasswordInput
        required
        label="Password"
        placeholder="Your password"
        mt="md"
        {...form.getInputProps("password")}
      />

      <Group position="apart" mt="lg">
        <Checkbox label="Remember me" sx={{ lineHeight: 1 }} />

        <Anchor<"a">
          onClick={(event) => event.preventDefault()}
          href="#"
          size="sm"
        >
          Forgot password?
        </Anchor>
      </Group>

      <Button fullWidth mt="xl" type="submit">
        Sign in
      </Button>
    </Form>
  );
};

export default LoginForm;
