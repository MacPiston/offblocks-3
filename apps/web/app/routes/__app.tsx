import { Outlet } from "@remix-run/react";
import { LoaderFunction } from "@remix-run/node";
import { supabaseStrategy } from "@packages/supabase-utils";

import Layout from "../components/layout";
import { routes } from "@routing";

export const loader: LoaderFunction = async ({ request }) =>
  supabaseStrategy.checkSession(request, {
    failureRedirect: routes.auth.login,
  });

export default function AuthLayout() {
  return (
    <Layout>
      <Outlet />
    </Layout>
  );
}
