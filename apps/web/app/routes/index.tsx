import { LoaderFunction } from "@remix-run/node";
import { supabaseStrategy } from "@packages/supabase-utils";
import { routes } from "@routing";

export const loader: LoaderFunction = async ({ request }) =>
  supabaseStrategy.checkSession(request, {
    successRedirect: routes.home,
  });

export default function Landing() {
  return <div>landing</div>;
}
