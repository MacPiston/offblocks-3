import { Outlet } from "@remix-run/react";
import { LoaderFunction } from "@remix-run/node";
import { supabaseStrategy } from "@packages/supabase-utils";

import { routes } from "@routing";
import Layout from "@components/layout";

export const loader: LoaderFunction = async ({ request }) =>
  supabaseStrategy.checkSession(request, {
    successRedirect: routes.home,
  });

export default function AuthLayout() {
  return (
    <Layout>
      <Outlet />
    </Layout>
  );
}
