import { ActionFunction, LoaderFunction } from "@remix-run/node";
import { Text, Anchor, Container, Paper, Title } from "@mantine/core";

import LoginForm from "@components/form/login";
import { loginAction, loginLoader } from "@server/auth/login";
import { routes } from "@routing";

export const loader: LoaderFunction = loginLoader;

export const action: ActionFunction = loginAction;

export default function LoginPage() {
  return (
    <Container size={420} my={40}>
      <Title
        align="center"
        sx={(theme) => ({
          fontFamily: `Greycliff CF, ${theme.fontFamily}`,
          fontWeight: 900,
        })}
      >
        Welcome back!
      </Title>

      <Text color="dimmed" size="sm" align="center" mt={5}>
        Do not have an account yet?{" "}
        <Anchor href={routes.auth.register} size="sm">
          Create account
        </Anchor>
      </Text>

      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <LoginForm />
      </Paper>
    </Container>
  );
}
